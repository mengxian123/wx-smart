package com.example.wxsmart;

import com.example.wxsmart.pojo.Question;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;

@SpringBootTest
class WxSmartApplicationTests {

    @Resource
    private RestHighLevelClient client;

    @Value("${qa.index}")
    private String index;

    @Test
    void contextLoads() {
        long l = 10241232L;
        byte[] bytes = new byte[4];
        bytes[0] = (byte) (int) (0 & 0xFF);
        bytes[1] = (byte) (int) ((l & 0xFFFFFF) >> 16 & 0xFF);
        bytes[2] = (byte) (int) ((l & 0xFFFF) >> 8 & 0xFF);
        bytes[3] = (byte) (int) (l & 0xFF);
        /**
           0111 1111 1111
         & 1111 1111 1111
         = 0111 1111 1111
         8  1111
         补 1000
         反 1111
         原 1000
         */
        for (int i = 0; i < bytes.length; i++) {
            System.out.println(bytes[i]);
        }

    }

    @Test
    void search() throws IOException {
        Question question = null;
        SearchRequest searchRequest = new SearchRequest(index);

        searchRequest
                .source()
                .query(QueryBuilders.matchQuery("question", "今天"));

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);

            SearchHits searchHits = searchResponse.getHits();

            long total = searchHits.getTotalHits().value;

            if (total != 0){
                SearchHit[] hits = searchHits.getHits();
                String sourceAsString = hits[0].getSourceAsString();
                //question = JSON.parseObject(sourceAsString, Question.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
