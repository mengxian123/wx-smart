package com.example.wxsmart;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 29610
 */
@SpringBootApplication
@MapperScan("com.example.wxsmart.mapper.**")
public class WxSmartApplication {

    public static void main(String[] args) {
        SpringApplication.run(WxSmartApplication.class, args);
    }

}
