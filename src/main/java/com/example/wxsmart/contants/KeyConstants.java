package com.example.wxsmart.contants;

/**
 * @author Luojunxian
 * @data 2022/8/2 17:35
 * @classname KeyConstants
 */
public class KeyConstants {

    private KeyConstants() {}

    public static final String KEY_ADD = "add";
    public static final String KEY_SET = "set";
    public static final String KEY_DEL = "del";

    public static final String KEY_ADD_OPERATIONAL = "add [key] [value]";
    public static final String KEY_SET_OPERATIONAL = "set [key] [value]";
    public static final String KEY_DEL_OPERATIONAL = "del [key] [value]";

    public static final String KEY_ADD_SUCCESS = "The key add success";
    public static final String KEY_SET_SUCCESS = "The key set success";
    public static final String KEY_DEL_SUCCESS = "The key del success";

    public static final String KEY_IS_EXIST = "The key is exist";
    public static final String KEY_NO_EXIST = "The key no exist";
    public static final String VALUE_NO_EXIST = "The value no exist";

    public static final String KEY_IS_NULL = "The key is null";
    public static final String VALUE_IS_NULL = "The value is null";

    public static final String KEY_ERROR = "The operational is error";

}
