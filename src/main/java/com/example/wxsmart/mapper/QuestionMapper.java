package com.example.wxsmart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.wxsmart.pojo.Question;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Luojunxian
 * @data 2022/5/30 9:55
 * @classname QuestionMapper
 */
@Mapper
public interface QuestionMapper extends BaseMapper<Question> {

}
