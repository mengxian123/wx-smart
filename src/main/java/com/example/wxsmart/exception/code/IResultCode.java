package com.example.wxsmart.exception.code;

import java.io.Serializable;

/**
 * @author Luojunxian
 * @data 2022/9/25 20:26
 * @classname IResultCode
 */
public interface IResultCode extends Serializable {
    /**
     * 得到消息
     *
     * @return {@code String}
     */
    String getMessage();

    /**
     * 获取代码
     *
     * @return int
     */
    int getCode();
}