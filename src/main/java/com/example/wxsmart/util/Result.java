package com.example.wxsmart.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 29610
 */
@Data
@ApiModel(value = "Result返回体")
public class Result<T> implements Serializable {
    public final static String SUCCESS = "操作成功!";
    public final static String FAIL = "操作失败!";

    @ApiModelProperty("返回状态")
    private Boolean flag;

    @ApiModelProperty("返回数据")
    private T data;

    @ApiModelProperty("返回信息")
    private String msg;

    public static <T> Result<T> ok(){
        return new Result(true, "{}", SUCCESS);
    }

    public static <T> Result<T> ok(T data){
        return new Result(true, data, SUCCESS);
    }

    public static <T> Result<T> ok(T data, String msg){
        return new Result(true, data, msg);
    }

     public static <T> Result<T> fail(){
        return new Result(false, "{}", FAIL);
    }

    public static <T> Result<T> fail(T data){
        return new Result(false, data, FAIL);
    }

    public static <T> Result<T> fail(T data, String msg){
        return new Result(false, data, msg);
    }

    public Result(Boolean flag, T data, String msg) {
        this.flag = flag;
        this.data = data;
        this.msg = msg;
    }
}
