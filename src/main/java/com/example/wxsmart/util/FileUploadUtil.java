package com.example.wxsmart.util;

import com.example.wxsmart.pojo.FileDetailEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * @author Luojunxian
 * @data 2022/5/24 11:21
 * @classname FileUploadUtil
 */
@Slf4j
@Component
public class FileUploadUtil {
    /**
     * servlet:
     *     multipart:
     *       max-file-size: 20MB
     *       max-request-size: 20MB
     */

    /**
     * /D:/AppIdea/springboot_2_24/springboot_ssmp/target/classes/static
     */
    public static String upPath;

    public final static String rootPath = System.getProperty("user.dir");

    static {
        try {
            upPath = ResourceUtils.getURL("classpath:").getPath() + "static";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static final Integer FILE_CREATE_TIME = 1;
    public static final Integer FILE_UPDATE_TIME = 2;
    public static final Integer FILE_ACCESS_TIME = 3;

    /**
     * 将B转换为MB/KB,四舍五入,保留两位小数
     *
     * @param fileSize 文件大小 字节/B
     * @return String 文件大小 大于1MB时返回单位为MB,小于为KB
     */
    public static String getFileSize(Long fileSize) {
        BigDecimal size = new BigDecimal(fileSize.toString());

        BigDecimal result = size.divide(new BigDecimal(1024 * 1024), 2, RoundingMode.HALF_UP);

        if (result.compareTo(new BigDecimal("1")) < 0) {
            return size.divide(new BigDecimal(1024), 1, RoundingMode.HALF_UP) + "KB";
        }
        return result + "MB";
    }

    /**
     * 获取文件file的创建/修改/访问时间
     *
     * @param file     文件
     * @param fileTime 获取的类型
     * @return String
     */
    public static String getFileTime(File file, Integer fileTime) {
        if (file == null) {
            return null;
        }

        BasicFileAttributes attr = null;
        try {
            Path path = file.toPath();
            attr = Files.readAttributes(path, BasicFileAttributes.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Instant instant;

        try {
            switch (fileTime) {
                case 1:
                    // 创建时间
                    instant = attr.creationTime().toInstant();
                    break;
                case 2:
                    // 修改时间
                    instant = attr.lastModifiedTime().toInstant();
                    break;
                case 3:
                    // 访问时间
                    instant = attr.lastAccessTime().toInstant();
                    break;
                default:
                    throw new RuntimeException("fileTime参数不存在!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault()).format(instant);
    }

    public static FileDetailEntity getFileDetail(File file) {
        return FileDetailEntity
                .builder()
                .fileName(file.getName())
                .filePath(file.getParentFile().getAbsolutePath())
                .fileSize(getFileSize(file.length()))
                .fileCreateTime(getFileTime(file, FILE_CREATE_TIME))
                .fileUpdateTime(getFileTime(file, FILE_UPDATE_TIME))
                .fileAccessTime(getFileTime(file, FILE_ACCESS_TIME))
                .build();
    }


    public static void main(String[] args) {
        log.info("{}", getFileSize(6011211L));
    }

}
