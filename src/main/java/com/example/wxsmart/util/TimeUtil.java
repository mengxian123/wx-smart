package com.example.wxsmart.util;


/**
 * @author 29610
 */
public class TimeUtil {

    /**
     * 方法说明：秒数 转小时和分钟和秒
     * 创建时间：2021-12-8 17:59:17
     * By 罗君贤
     *
     * @param second 第二个
     * @return {@code String}
     */
    public static String formatHourMinSecond(int second) {
        if (second == 1 || second < 0) {
            second = 0;
        }

        int min = second / 60;
        int s = second % 60;

        int m = min % 60;
        int hours = min / 60;

        StringBuilder str = new StringBuilder();
        if (hours != 0) {
            str.append(hours).append("h ");
        }
        if (m != 0 || hours != 0) {
            str.append(m).append("m ");
        }

        str.append(s).append("s ");
        return str.toString();
    }

    /**
     * 将 毫秒 转换为时分秒毫秒形式
     *
     * @param milliseconds 毫秒
     * @return {@code String}
     */
    public static String formatHourMinSecondMillisecond(Long milliseconds) {
        if (milliseconds == null || milliseconds < 0L) {
            milliseconds = 0L;
        }

        long second = milliseconds / 1000;
        long l = milliseconds % 1000;

        return (second == 0 ? l : formatHourMinSecond((int) second) + l) + "ms ";
    }
}
