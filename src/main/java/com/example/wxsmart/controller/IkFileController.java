package com.example.wxsmart.controller;

import com.example.wxsmart.config.IkFilePathConfig;
import com.example.wxsmart.dto.request.IkFileRequest;
import com.example.wxsmart.resources.IkFileResources;
import com.example.wxsmart.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.File;

/**
 * @author Luojunxian
 * @data 2022/5/30 10:30
 * @classname IkFileController
 */
@Slf4j
@Api(tags = "ik分词文件的curd")
@RestController
@RequestMapping("/ik/file")
public class IkFileController {

    @Resource
    private IkFilePathConfig ikFilePath;

    @Resource
    private IkFileResources ikFileResources;

    @GetMapping("/read")
    @ApiOperation("文件的读取")
    public Result readFile(@RequestBody IkFileRequest request) {
        String path = ikFilePath.getPath(request.getFileName());

        if (null == path) {
            return Result.fail("文件:" + request.getFileName() + "不存在!");
        }

        log.info("Ik:{}开始读取...", path);

        return Result.ok(ikFileResources.read(new File(path)));
    }

    @PostMapping("/add")
    @ApiOperation("文件的修改")
    public Result addFile(@RequestBody IkFileRequest request) {
        String path = ikFilePath.getPath(request.getFileName());

        if (null == path) {
            return Result.fail("文件:" + request.getFileName() + "不存在!");
        }

        log.info("Ik:{}开始修改...", path);

        return Result.ok(ikFileResources.add(new File(path), request.getValues()));
    }

    @PostMapping("/delete")
    @ApiOperation("文件的删除")
    public Result deleteFile(@RequestBody IkFileRequest request) {
        String path = ikFilePath.getPath(request.getFileName());

        if (null == path) {
            return Result.fail("文件:" + request.getFileName() + "不存在!");
        }

        log.info("Ik:{}开始删除...", path);

        return Result.ok(ikFileResources.delete(new File(path), request.getValues()));
    }


}
