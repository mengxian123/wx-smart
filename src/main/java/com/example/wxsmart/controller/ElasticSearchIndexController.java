package com.example.wxsmart.controller;


import com.example.wxsmart.dto.request.ElasticSearchCreateIndexRequest;
import com.example.wxsmart.resources.ElasticSearchIndexResources;
import com.example.wxsmart.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author Luojunxian
 * @data 2022/5/29 0:23
 * @classname HotelController
 */
@Api(tags = "ES")
@RestController
@RequestMapping("/elasticsearch")
public class ElasticSearchIndexController {

    @Value("${qa.index}")
    private String index;

    @Resource
    public ElasticSearchIndexResources elasticSearchIndexResources;

    @PostMapping("/createIndex")
    @ApiOperation("创建es索引")
    public Result<String> createIndex(@RequestBody ElasticSearchCreateIndexRequest request) {
        request.setIndex(index);
        elasticSearchIndexResources.createIndex(request);
        return Result.ok();
    }

    @PostMapping("/deleteIndex")
    @ApiOperation("删除es索引")
    public Result<String> deleteIndex(){
        elasticSearchIndexResources.deleteIndex(index);
        return Result.ok();
    }

    @GetMapping("/existsIndex")
    @ApiOperation("判断es索引是否存在")
    public Result<Boolean> existsIndex(){
        return elasticSearchIndexResources.existsIndex(index);
    }
}
