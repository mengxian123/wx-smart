package com.example.wxsmart.controller;

import com.example.wxsmart.pojo.User;
import com.example.wxsmart.util.JwtUtil;
import com.example.wxsmart.util.Result;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Luojunxian
 * @data 2022/7/7 13:52
 * @classname LoginController
 */
@Api(tags = "鉴权登录")
@Slf4j
@RestController
@RequestMapping("/login")
public class LoginController {

    @Resource
    private JwtUtil jwtUtil;

    @PostMapping
    public Result<String> login(@Validated @RequestBody User user){
        if (user.getName().equals("admin") && user.getPassword().equals("123456")){
            user.setId(1);
            String token = JwtUtil.createToken(user);
            return Result.ok(token);
        }
        return Result.fail("登录失败...");
    }
}
