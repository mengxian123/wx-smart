package com.example.wxsmart.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.wxsmart.dto.request.QuestionPageRequest;
import com.example.wxsmart.pojo.Question;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Luojunxian
 * @data 2022/5/30 9:56
 * @classname QuestionService
 */
public interface QuestionService extends IService<Question> {

    /**
     * 添加/修改
     *
     * @param question 问题
     * @return boolean
     */
    boolean addOrUpdate(Question question);

    /**
     * 删除
     *
     * @param questionId 问题id
     * @return boolean
     */
    boolean delete(Integer questionId);

    /**
     * 批量添加
     *
     * @param questionList 问题列表
     * @return boolean
     */
    @Transactional(rollbackFor = Exception.class)
    boolean addOrUpdateList(List<Question> questionList);

    /**
     * 分页查询
     *
     * @param request 请求
     * @return {@code IPage<Question>}
     */
    IPage<Question> getPagedList(QuestionPageRequest request);

    /**
     * 获取list
     *
     * @param idList id列表
     * @return {@code List<Question>}
     */
    List<Question> getQuestionList(List<Integer> idList);

    /**
     * 测试
     */
    void test();

}
