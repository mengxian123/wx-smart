package com.example.wxsmart.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.example.wxsmart.util.excel.ExcelExport;
import com.example.wxsmart.util.excel.ExcelImport;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Luojunxian
 * @data 2022/5/30 9:52
 * @classname Question
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("question")
@ApiModel("问答表")
public class Question implements Serializable {

    @ApiModelProperty("id")
    @TableId("id")
    @ExcelExport(value = "id", example = "1")
    @ExcelImport("id")
    @Min(1)
    Integer id;

    @ApiModelProperty("问题")
    @ExcelExport(value = "问题", example = "How are you?")
    @ExcelImport("问题")
    @NotBlank(message = "问题不能为空")
    String question;

    @ApiModelProperty("回答")
    @ExcelExport(value = "回答", example = "I'm fine.")
    @ExcelImport("回答")
    @NotBlank(message = "回答不能为空")
    String answer;

    @ApiModelProperty("数量")
    @ExcelExport(value = "数量", example = "0")
    @ExcelImport("数量")
    @DecimalMin(value = "0", message = "数量不能小于0")
    @NotNull(message = "数量不能为空")
    Integer count;

    @ApiModelProperty("创建时间")
    @ExcelExport(value = "创建时间", example = "2022-6-21 14:55:37")
    @ExcelImport("创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @ExcelExport(value = "更新时间", example = "2022-6-21 14:55:56")
    @ExcelImport("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    LocalDateTime updateTime;

    @ApiModelProperty("是否删除 0否 1是")
    @TableLogic
    boolean isDeleted;

    public String toStrings() {
        return "问题：" + question + "\r\n" +
                "回答：" + answer;
    }
}
