package com.example.wxsmart.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Luojunxian
 * @data 2022/8/22 16:57
 * @classname Video
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Video {

    Integer vod_id;

    String vod_name;

    Integer type_id;

    String type_name;

    String vod_en;

    Date vod_time;

    String vod_remarks;

    String vod_play_from;
}
