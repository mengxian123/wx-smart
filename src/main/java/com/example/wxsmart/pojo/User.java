package com.example.wxsmart.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Luojunxian
 * @data 2022/7/7 13:55
 * @classname User
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户类")
public class User {

    @ApiModelProperty("用户id")
    Integer id;

    @ApiModelProperty("用户名")
    @NotBlank(message = "用户名不能为空")
    String name;

    @ApiModelProperty("用户密码")
    @NotBlank(message = "用户密码不能为空")
    String password;
}
