package com.example.wxsmart.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * 文件详细信息实体
 *
 * @author Luojunxian
 * @data 2022/5/27 11:24
 * @classname FileDetailEntity
 * @date 2022/08/03
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class FileDetailEntity {

    /**
     * 文件名
     */
    @ApiModelProperty("文件名")
    String fileName;

    /**
     * 文件大小
     */
    @ApiModelProperty("文件大小")
    String fileSize;

    /**
     * 文件行数
     */
    @ApiModelProperty("文件行数")
    Integer fileRow;

    /**
     * 文件路径
     */
    @ApiModelProperty("文件路径")
    String filePath;

    /**
     * 文件创建时间
     */
    @ApiModelProperty("文件创建时间")
    String fileCreateTime;

    /**
     * 文件修改时间
     */
    @ApiModelProperty("文件修改时间")
    String fileUpdateTime;

    /**
     * 文件访问时间
     */
    @ApiModelProperty("文件访问时间")
    String fileAccessTime;

    /**
     * 文件内容
     */
    @ApiModelProperty("文件内容")
    List<String> fileContent;
}
