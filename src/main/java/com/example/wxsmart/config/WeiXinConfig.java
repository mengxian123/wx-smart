package com.example.wxsmart.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author Luojunxian
 * @data 2022/5/31 9:12
 * @classname WeiXinConfig
 */
@Data
@Component
@ConfigurationProperties(prefix = "wx")
public class WeiXinConfig implements Serializable {

    String sToken;

    String sCorpID;

    String secret;

    String sEncodingAESKey;

    String touser;

    String agentId;

    String access_token_url;

    String send_message_url;

}
