package com.example.wxsmart.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Luojunxian
 * @data 2022/5/29 0:16
 * @classname elasticSearchConfig
 */
@Configuration
public class ElasticsearchConfig {

    @Value("${elasticsearch.host}")
    public String host;

    /**
     * 配置es链接
     *
     * @return {@code RestHighLevelClient}
     */
    @Bean
    public RestHighLevelClient restHighLevelClient(){
        return new RestHighLevelClient(RestClient.builder(HttpHost.create(host)));
    }
}
