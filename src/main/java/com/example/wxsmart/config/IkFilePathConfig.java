package com.example.wxsmart.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Luojunxian
 * @data 2022/5/30 11:24
 * @classname IkFilePath
 */
@Component
public class IkFilePathConfig {
    @Value("${elasticsearch.ext}")
    private String ext;

    @Value("${elasticsearch.stopWord}")
    private String stopWord;

    /**
     * 获取路径
     *
     * @param path 路径
     * @return {@code String}
     */
    public String getPath(String path) {
        String result;
        switch (path) {
            case "ext":
                result = ext;
                break;
            case "stopWord":
                result = stopWord;
                break;
            default:
                result = null;
        }

        return result;
    }
}
