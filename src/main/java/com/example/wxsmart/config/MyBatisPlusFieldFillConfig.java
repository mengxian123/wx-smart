package com.example.wxsmart.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author Luojunxian
 * @data 2022/6/21 14:57
 * @classname MyBatisPlusFieldFillConfig
 */
@Slf4j
@Component
public class MyBatisPlusFieldFillConfig implements MetaObjectHandler {
    /**
     * 插入时填充
     *
     * @param metaObject 元对象
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("MyBatisPlus:start insert fill.......");
        this.strictInsertFill(metaObject, "createTime", LocalDateTime.class, LocalDateTime.now());
        this.strictInsertFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now());
    }

    /**
     * 更新时填充
     *
     * @param metaObject 元对象
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("MyBatisPlus:start update fill.......");
        // 该更新填充在有值不为null时失效，原因未知
        /*this.strictUpdateFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now());*/

        // 该填充策略已过时(但能用)
        this.setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
    }

}
