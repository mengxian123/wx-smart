package com.example.wxsmart.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * @author Luojunxian
 * @data 2022/6/22 10:54
 * @classname WebMvcConfig
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * 拦截器控制
     *
     * @param registry 注册表
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //InterceptorRegistration interceptor = registry.addInterceptor(new LoginInterceptor());

        //interceptor
        //.addPathPatterns("/**")
        //.excludePathPatterns("/css/**", "/js/**", "/images/**", "/font/**", "/login", "/", "/question/list", "/index.html");
    }

    /**
     * 静态资源控制
     *
     * @param registry 注册表
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        WebMvcConfigurer.super.addResourceHandlers(registry);
    }

    /**
     * 页面控制
     *
     * @param registry 注册表
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        // 问答数据管理页面跳转
        registry.addViewController("").setViewName("index.html");
    }

    /**
     * 配置跨域
     *
     * @param registry 注册表
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry
                // 允许跨域的路由
                .addMapping("/**")
                // 允许跨域请求的域名
                .allowedOriginPatterns("*")
                // 允许跨域请求的方法
                .allowedMethods("*")
                // 允许跨域的请求头
                .allowedHeaders("*")
                // 允许跨域的的时间
                .maxAge(3600)
                // 允许证书
                .allowCredentials(true);
    }
}

