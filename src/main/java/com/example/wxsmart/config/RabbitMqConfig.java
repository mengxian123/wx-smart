package com.example.wxsmart.config;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Luojunxian
 * @data 2022/5/20 15:38
 * @classname RabbitMqConfig
 */
@Configuration
public class RabbitMqConfig {

    /**
     * 消息转换器
     * 配置json流
     * 1.防止乱码
     * 2.可直接用对象入参接参
     *
     * @return {@code MessageConverter}
     */
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
