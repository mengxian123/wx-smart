package com.example.wxsmart.timer;

import com.example.wxsmart.resources.VideoResources;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

/**
 * @author Luojunxian
 * @data 2022/8/22 16:43
 * @classname SynchronizationJob 同步定时器
 */
@Component
@EnableScheduling
@AllArgsConstructor
public class SynchronizationJob {

    private VideoResources videoResources;

    /**
     * 视频同步,每5分钟同步一次
     */
    //@Scheduled(cron = "0 0/1 * * * ?")
    public void videoJob(){
        videoResources.saveOrUpdateVideoQuestion();
    }
}
