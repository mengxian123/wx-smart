package com.example.wxsmart.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Savepoint;
import java.util.List;

/**
 * @author Luojunxian
 * @data 2022/5/30 10:33
 * @classname IkFileAddRequest
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("ik文件添加请求类")
public class IkFileRequest {

    /**
     * 文件名
     */
    @ApiModelProperty(value = "文件名")
    String fileName;

    /**
     * 需要添加的值
     */
    @ApiModelProperty("需要添加的值")
    List<String> values;
}
