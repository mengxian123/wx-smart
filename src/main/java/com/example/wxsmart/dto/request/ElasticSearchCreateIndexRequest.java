package com.example.wxsmart.dto.request;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Luojunxian
 * @data 2022/5/29 13:34
 * @classname ElasticSearchCreateIndexRequest
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("es索引创建请求类")
public class ElasticSearchCreateIndexRequest {
    /**
     * 索引名称
     */
    @ApiModelProperty("索引名称")
    String index;

    /**
     * 索引参数DLS
     */
    @ApiModelProperty("索引参数DLS")
    JSONObject source;
}
