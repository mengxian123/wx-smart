package com.example.wxsmart.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Luojunxian
 * @data 2022/6/1 13:27
 * @classname QuestionPageRequest
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("问答查询请求类")
public class QuestionPageRequest implements Serializable {

    @ApiModelProperty("页码")
    Integer pageIndex = 1;

    @ApiModelProperty("页数")
    Integer pageSize = 5;

    @ApiModelProperty("问题")
    String question;

    @ApiModelProperty("问题id")
    Integer questionId;
}
