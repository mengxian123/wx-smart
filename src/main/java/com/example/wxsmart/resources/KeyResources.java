package com.example.wxsmart.resources;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.wxsmart.contants.KeyConstants;
import com.example.wxsmart.pojo.Question;
import com.example.wxsmart.service.QuestionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Luojunxian
 * @data 2022/8/2 17:33
 * @classname KeyResources
 */
@Slf4j
@Component
@AllArgsConstructor
public class KeyResources {
    private QuestionService questionService;

    /**
     * 添加键
     *
     * @param question 问题
     * @return {@code String}
     */
    public String addKey(Question question) {
        Question one = getQuestionByKey(question.getQuestion());
        if (one != null) {
            return KeyConstants.KEY_IS_EXIST;
        } else {
            if (questionService.addOrUpdate(question)) {
                return KeyConstants.KEY_ADD_SUCCESS;
            }
            return KeyConstants.KEY_ERROR;
        }
    }

    /**
     * 设置键
     *
     * @param question 问题
     * @return {@code String}
     */
    public String setKey(Question question) {
        Question one = getQuestionByKey(question.getQuestion());
        if (one == null) {
            return KeyConstants.KEY_NO_EXIST;
        } else {
            one.setAnswer(question.getAnswer());
            if (questionService.addOrUpdate(one)) {
                return KeyConstants.KEY_SET_SUCCESS;
            }
            return KeyConstants.KEY_ERROR;
        }
    }

    /**
     * 删除键
     *
     * @param question 问题
     * @return {@code String}
     */
    public String deleteKey(Question question) {
        Question one = getQuestionByKey(question.getQuestion());
        if (one == null) {
            return KeyConstants.KEY_NO_EXIST;
        } else {
            one.setAnswer(question.getAnswer());
            if (questionService.addOrUpdate(question)) {
                return KeyConstants.KEY_DEL_SUCCESS;
            }
            return KeyConstants.KEY_ERROR;
        }
    }

    /**
     * 根据key获取问答
     *
     * @param key 关键
     * @return {@code Question}
     */
    private Question getQuestionByKey(String key) {
        return questionService.getOne(Wrappers.<Question>lambdaQuery().eq(Question::getQuestion, key).last(" limit 1"));
    }

    /**
     * 获取操作
     *
     * @param message 消息
     * @return {@code String}
     */
    public String getOperational(String message) {
        String operational = message.length() >= 3 ? message.substring(0, 3) : "";

        switch (operational) {
            case KeyConstants.KEY_ADD:
                return KeyConstants.KEY_ADD;
            case KeyConstants.KEY_SET:
                return KeyConstants.KEY_SET;
            case KeyConstants.KEY_DEL:
                return KeyConstants.KEY_DEL;
            default:
                return null;
        }
    }

    /**
     * 命令操作
     *
     * @param operational 操作
     * @param message     消息
     * @return {@code String}
     */
    public String operational(String operational, String message) {
        // 判断长度是否为3,否则返回对应的操作说明
        if (message.length() == 3) {
            log.info("{}操作说明", operational);
            switch (operational) {
                case KeyConstants.KEY_ADD:
                    return KeyConstants.KEY_ADD_OPERATIONAL;
                case KeyConstants.KEY_SET:
                    return KeyConstants.KEY_SET_OPERATIONAL;
                case KeyConstants.KEY_DEL:
                    return KeyConstants.KEY_DEL_OPERATIONAL;
                default:
                    return KeyConstants.KEY_ERROR;
            }
        }

        // 获取key-value,并校验
        int index = message.indexOf(" ", 4);

        if (index == -1 && message.length() <= 4) {
            return KeyConstants.KEY_NO_EXIST;
        }

        if (index == -1 || index >= message.length()) {
            return KeyConstants.VALUE_NO_EXIST;
        }

        String question = message.substring(3, index);
        String answer = message.substring(index + 1);

        if (question.trim().length() == 0) {
            return KeyConstants.KEY_IS_NULL;
        }

        if (answer.trim().length() == 0) {
            return KeyConstants.VALUE_IS_NULL;
        }

        // 组装key-value,并进行相应的操作
        Question build = Question.builder().question(question).answer(answer).build();

        log.info("{}操作,{}", operational, build.toString());

        switch (operational) {
            case KeyConstants.KEY_ADD:
                return addKey(build);
            case KeyConstants.KEY_SET:
                return setKey(build);
            case KeyConstants.KEY_DEL:
                return deleteKey(build);
            default:
                return KeyConstants.KEY_ERROR;
        }
    }
}
