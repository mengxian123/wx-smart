# wx-smart

#### 介绍
实现了简单的企业微信智能问答
流程 企微应用发送消息 - 后台接收消息，用elasticsearch进行ik分词搜索，返回结果 - 调用企微接口进行消息返回

#### 软件架构
软件架构说明
springboot，mysql，elasticsearch，redis，rabbitmq

#### 安装教程

1.  拉取代码
2.  配置mysql5.7，elasticsearch7.2（ik7.2），redis（latest），rabbitmq（latest），application信息，安装可参考https://blog.csdn.net/Mis_xian/article/details/125294330 （使用docker安装比较快一点）
3.  可直接启动，也可用docker
4.  企微应用后台配置服务器回调地址（用于接发消息）

#### 使用说明

1.  后台页面：ip:8088
2.  接口文档：ip:8088/swagger-ui.html
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 联系
如果代码中有错误，有不理解或者有建议的小伙伴可以加我q-2961007424或v-xian2961007424进行联系